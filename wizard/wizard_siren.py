# © 2018 Le Filament (<https://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import requests

from odoo import api, fields, models

URL = "https://data.opendatasoft.com/api/records/1.0/"\
    "search/?dataset=sirene_v3%40public&q={request}&rows=100"


class SirenWizard(models.TransientModel):
    _name = 'siren.wizard'
    _description = 'Get values from companies'

    # Default functions
    @api.model
    def _default_name(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).name

    @api.model
    def _default_partner(self):
        return self.env.context.get('active_id')

    # Fields
    name = fields.Char(string='Entreprise', default=_default_name)
    city = fields.Char(string='Ville')
    company_lines = fields.One2many('siren.wizard.company',
                                    'wizard_id', string="Résultats",)
    partner_id = fields.Integer('Partner', default=_default_partner)

    # Action
    @api.model
    def _prepare_partner_from_data(self, data):
        return {
            'name': data.get('denominationunitelegale'),
            'street': data.get('adresseetablissement', False),
            'zip': data.get('codepostaletablissement', False),
            'city': data.get('libellecommuneetablissement', False),
            'siren': data.get('siren', False),
            'siret': data.get('siret', False),
            'categorie': data.get('categorieentreprise', False),
            'date_creation': data.get('datecreationunitelegale',
                                      False),
            'ape': data.get('activiteprincipaleunitelegale', False),
            'lib_ape': data.get('divisionunitelegale', False),
            'forme_juridique': data.get('naturejuridiqueunitelegale', False),
            'effectif': data.get('trancheeffectifsunitelegale', 0),
            'ess': (True if data.get('economiesocialesolidaireunitelegale')
                    else False),
            'etat_etablissement': data.get(
                'etatadministratifetablissement',
                False),
        }

    def get_company_lines(self):
        # Get request
        r = requests.get(URL.format(request=self.name))
        # Serialization request to JSON
        companies = r.json()
        # Fill new company lines
        companies_vals = []
        for company in companies['records']:
            res = self._prepare_partner_from_data(company['fields'])
            companies_vals.append((0, 0, res))
        self.company_lines.unlink()
        self.company_lines = companies_vals
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'siren.wizard',
            'res_id': self.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }


class SirenWizardCompanies(models.TransientModel):
    _name = 'siren.wizard.company'
    _description = 'Companies Selection'

    # Fields
    wizard_id = fields.Many2one('siren.wizard', string='Wizard',)
    name = fields.Char(string='Nom')
    street = fields.Char(string='Rue')
    zip = fields.Char(string='CP')
    city = fields.Char(string='Ville')

    forme_juridique = fields.Char("Forme Juridique")
    siren = fields.Char("SIREN")
    siret = fields.Char("SIRET")
    ape = fields.Char("Code APE")
    lib_ape = fields.Char("Libellé APE")
    date_creation = fields.Date("Date de création")
    effectif = fields.Char("Effectif")
    ess = fields.Boolean("ESS", default=False)
    categorie = fields.Char("Catégorie")
    etat_etablissement = fields.Char("Etat établissement")

    # Action
    @api.multi
    def update_partner(self):
        partner = self.env['res.partner'].browse(self.wizard_id.partner_id)
        partner.write({
            'name': self.name,
            'street': self.street,
            'zip': self.zip,
            'city': self.city,
            'forme_juridique': self.forme_juridique,
            'siren': self.siren,
            'siret': self.siret,
            'ape': self.ape,
            'lib_ape': self.lib_ape,
            'date_creation': self.date_creation,
            'effectif': self.effectif,
            'ess': self.ess,
            'categorie': self.categorie,
        })
