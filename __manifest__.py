# © 2018 Le Filament (<https://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Le Filament - SIREN',
    'summary': "Recherche dans la base SIREN",
    'version': '10.0.1.0.0',
    'license': 'AGPL-3',
    'author': 'LE FILAMENT',
    'category': 'Partner',
    'depends': ['base'],
    'contributors': [
        'Benjamin Rivier <benjamin@le-filament.com>',
        'Rémi Cazenave <remi@le-filament.com>',
    ],
    'website': 'https://www.le-filament.com',
    'data': [
        'wizard/wizard_siren.xml',
        'views/res_partner.xml',
    ],
    'qweb': [],
}
