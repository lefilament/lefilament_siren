# © 2018 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models


class PartnerSiren(models.Model):
    _inherit = 'res.partner'

    # Fields
    forme_juridique = fields.Char("Forme Juridique")
    siren = fields.Char("SIREN")
    siret = fields.Char("SIRET")
    ape = fields.Char("Code APE")
    lib_ape = fields.Char("Libellé APE")
    date_creation = fields.Date("Date de création")
    effectif = fields.Char("Effectif")
    ess = fields.Boolean("ESS")
    categorie = fields.Char("Catégorie")
