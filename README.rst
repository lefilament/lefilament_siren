.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


======================
Le Filament - SIREN
======================

Description
===========

Permet d'entrer les informations d'adresse, de contact d'une entreprise en utilisant la base SIRENE data.opendatasoft.com

Le module crée un wizard disponible pour les société qui met à jour les informations suivantes :

* Nom de l'entreprise
* Adresse
* Code Postal
* Ville
* Forme Juridique
* Siren et Siret
* Code et libellé APE
* Date de création
* Effectif
* Catégorie d'entreprise
* Date et libellé ESS

Ce module utilise l'api mise à disposition par l'INSEE et disponible à l'adresse suivante :  https://data.opendatasoft.com/api/records/1.0/search/

Ce module a été traduit en anglais, la partie ESS enlevée (car considérée pas assez générique) et proposé à l'OCA en v10 (https://github.com/OCA/l10n-france/pull/163) et v12 (https://github.com/OCA/l10n-france/pull/173)

Usage
=====

Un bouton "Pré-remplir / Mettre à jour" apparaît sur les fiches société.

Par défaut, le champ de recherche prend le nom de la société comme variable. Pour faire des recherches plus précises, vous pouvez ajouter la ville de la société puis cliquer sur "Rechercher".

Une liste de sociétés apparaît. Vous pouvez alors cliquer sur la société pour voir ses informations ou directement sélectionner la société depuis la treeview. Une fois la société sélectionnée, le wizard disparait et les informations de la société sont mise à jour dans l'onglet "Infos légales".


Credits
=======

Contributors
------------

* Benjamin Rivier <benjamin@le-filament.com>
* Remi Cazenave <remi@le-filament.com>


Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by `Le Filament <https://le-filament.com>`_